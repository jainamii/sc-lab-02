package Controller;

import Model.BankAccount;
import View.InvestmentFrame;
public class Controller {
	
	BankAccount account;
	InvestmentFrame inv;
	private static final double INITIAL_BALANCE = 1000;  
	
	public Controller(BankAccount account,InvestmentFrame inv){
		this.account = account;
		this.inv = inv;
		account = new BankAccount(INITIAL_BALANCE);
	     
	}

	public void deposit(double amount){
		account.deposit(amount);
	}

	public void withdraw(double amount){
		account.withdraw(amount);
	}
	public double getBalnace(){
		return account.getBalance();
	}

}
